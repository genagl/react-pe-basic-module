import React, { Component } from "react";

class FreeHTMLWidget extends Component {
  render() {
    return /*#__PURE__*/React.createElement("div", {
      className: this.props.container_class,
      dangerouslySetInnerHTML: {
        __html: this.props.html
      }
    });
  }

}

export default FreeHTMLWidget;