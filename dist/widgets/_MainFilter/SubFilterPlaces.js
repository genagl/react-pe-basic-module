function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Icon } from "@blueprintjs/core";
import PlaceTypesFilters from "./PlaceTypesFilters";

class SubFilterPlaces extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onClose", bool => {
      this.setState({
        isOpen: bool
      });
    });

    _defineProperty(this, "onClick", evt => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    });

    this.state = {
      isOpen: false,
      sPlaceTypes: this.props.sPlaceTypes
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.sPlaceTypes !== this.state.sPlaceTypes) {
      this.setState({
        sPlaceTypes: nextProps.sPlaceTypes
      });
    }
  }

  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "pointer",
      onClick: this.onClick
    }, /*#__PURE__*/React.createElement(Icon, {
      icon: !this.state.isOpen ? "chevron-down" : "chevron-up"
    })), /*#__PURE__*/React.createElement(PlaceTypesFilters, {
      isOpen: this.state.isOpen,
      onOpen: this.onClose,
      placeTypes: this.props.placeTypes,
      sPlaceTypes: this.state.sPlaceTypes,
      onSelect: this.props.onSelect
    }));
  }

}

export default SubFilterPlaces;