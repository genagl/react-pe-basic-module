function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React, { Component } from "react";
import { __ } from "react-pe-utilities";
import { concatRouting } from "react-pe-layouts";
import { getWidgets } from "react-pe-utilities";

class Aside extends Component {
  render() {
    const aside = this.props.route ? concatRouting().filter(e => this.props.route.split("/")[1] == e.route)[0].aside : []; // console.log( aside );

    const __widgets = aside.map((e, i) => {
      if (getWidgets[e.component]) {
        const _Widget = getWidgets[e.component].default;
        return /*#__PURE__*/React.createElement("div", {
          className: "aside-widget col-12",
          key: i
        }, /*#__PURE__*/React.createElement("div", {
          className: "aside-widget-title"
        }, __(e.title)), /*#__PURE__*/React.createElement(_Widget, _extends({}, this.props, e.args)));
      }

      return /*#__PURE__*/React.createElement("small", {
        className: "m-5 text-secondary text-center"
      }, __("No widget exists: ") + e.component);
    });

    return /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, __widgets);
  }

}

export default Aside;