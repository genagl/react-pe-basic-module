function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { __ } from "react-pe-utilities";

class SearchBlock extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      isOpen: false,
      search: ""
    });

    _defineProperty(this, "onToggle", () => {
      console.log(this.state.isOpen);
      this.setState({
        isOpen: !this.state.isOpen
      });
    });

    _defineProperty(this, "onSearch", evt => {
      this.setState({
        search: evt.currentTarget.value
      });
    });

    _defineProperty(this, "onKey", evt => {
      if (evt.key === "Enter") {
        this.setState({
          isOpen: false
        });
        this.props.history.push("/search", {
          s: this.state.search
        });
      }
    });
  }

  render() {
    // console.log(this.props);
    return /*#__PURE__*/React.createElement("div", {
      className: "sb-container"
    }, /*#__PURE__*/React.createElement("div", {
      className: `sb-cont ${this.state.isOpen ? "open" : ""}`
    }, /*#__PURE__*/React.createElement("input", {
      type: "text",
      onChange: this.onSearch,
      onKeyPress: this.onKey,
      placeholder: __("Введите поисковый запрос")
    })), /*#__PURE__*/React.createElement("div", {
      className: "sb-icon-search ",
      onClick: this.onToggle
    }));
  }

}

export default withRouter(SearchBlock);