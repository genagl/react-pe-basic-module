function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import ReactDOM from "react-dom";
import { __ } from "react-pe-utilities";
import { LayoutIcon } from 'react-pe-useful';
import { NavLink } from "react-router-dom";
import { profile } from "react-pe-layouts";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import { withRouter } from "react-router";
import { isCapability } from "react-pe-utilities";
import { login, template } from "react-pe-layouts";
import { initArea } from "react-pe-utilities";

class CurrentUserPanelWidget extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onMouseLeaveHandler", e => {
      const domNode = ReactDOM.findDOMNode(this);

      if (!domNode || !domNode.contains(e.target)) {
        this.setState({
          press: false,
          height: 0
        });
      }
    });

    _defineProperty(this, "unlogined", () => {
      return initArea("unlogin_panel", { ...this.props
      }, /*#__PURE__*/React.createElement(NavLink, {
        className: "layout-left-btn " + (this.state.press ? " active " : ""),
        to: login
      }, /*#__PURE__*/React.createElement("div", {
        className: "layout-menu-icon"
      }, /*#__PURE__*/React.createElement(LayoutIcon, {
        src: "fas fa-sign-in-alt",
        className: "left-menu-icon"
      })), /*#__PURE__*/React.createElement("div", {
        className: `layout-menu-left-label ${this.state.hover ? "hover" : null}`
      }, __("log in"))));
    });

    _defineProperty(this, "logined", () => {
      const {
        user
      } = this.props;
      const profile_routing = profile();
      let profile_menu;

      if (profile_routing.length > 0) {
        profile_menu = profile_routing.map((e, i) => {
          const isRole = isCapability(e.capability, this.props.user);
          if (isRole) return "";
          let children;

          if (Array.isArray(e.children) && e.children.length > 0) {
            children = /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
              className: "lmenu-children"
            }));
          } else {}

          return /*#__PURE__*/React.createElement("li", {
            className: "lmenu",
            key: i
          }, children, /*#__PURE__*/React.createElement(NavLink, {
            className: "",
            activeClassName: "active",
            to: `/${e.route}`
          }, /*#__PURE__*/React.createElement(LayoutIcon, {
            isSVG: true,
            src: e.icon,
            className: "personal-menu__icon mr-3"
          }), __(e.title)));
        });
      } else {
        profile_menu = /*#__PURE__*/React.createElement("li", {
          className: "lmenu"
        }, /*#__PURE__*/React.createElement(NavLink, {
          className: "",
          activeClassName: "active",
          to: "/profile"
        }, /*#__PURE__*/React.createElement(LayoutIcon, {
          isSVG: true,
          src: "/assets/img/user.svg",
          className: "personal-menu__icon mr-3"
        }), __("edit profile")));
      }

      const login_panel = /*#__PURE__*/React.createElement("div", {
        className: "position-relative"
      }, /*#__PURE__*/React.createElement("div", {
        className: "layout-left-btn " + (this.state.press ? " active " : ""),
        onMouseDown: () => this.togglePress(!this.state.press)
      }, /*#__PURE__*/React.createElement("div", {
        className: "layout-menu-icon"
      }, /*#__PURE__*/React.createElement(LayoutIcon, {
        src: "fas fa-user",
        className: "left-menu-icon"
      })), /*#__PURE__*/React.createElement("div", {
        className: `layout-menu-left-label ${this.state.hover ? "hover" : null}`
      }, this.props.user.display_name)), /*#__PURE__*/React.createElement("div", {
        className: "logined-menu lm-widget",
        style: {
          height: this.state.height
        }
      }, /*#__PURE__*/React.createElement("ul", {
        id: "person_menu_widget"
      }, profile_menu, /*#__PURE__*/React.createElement("li", {
        onClick: this.logout,
        className: "lmenu exit"
      }, /*#__PURE__*/React.createElement(LayoutIcon, {
        isSVG: true,
        src: "/assets/img/logout.svg",
        className: "personal-menu__icon mr-3"
      }), __("logout")))));
      return /*#__PURE__*/React.createElement(React.Fragment, null, initArea("login_panel", { ...this.props
      }, login_panel));
    });

    _defineProperty(this, "onToggle", evt => {
      // console.log( document.getElementById("person_menu").clientHeight );
      this.setState({});
    });

    _defineProperty(this, "logout", () => {
      localStorage.removeItem("token");
      this.props.refetchUser();
    });

    _defineProperty(this, "togglePress", bool => {
      this.setState({
        press: bool,
        height: bool ? document.getElementById("person_menu_widget").clientHeight : 0
      });
    });

    this.state = {
      hover: false,
      press: false,
      height: 0,
      current: this.props.current
    };
  }

  componentDidMount() {
    document.body.addEventListener("click", this.onMouseLeaveHandler);
  }

  componentWillUnmount() {
    document.body.removeEventListener("click", this.onMouseLeaveHandler);
  }

  render() {
    return this.props.user ? this.logined() : this.unlogined();
  }

  toggleHover() {
    this.setState({
      hover: !this.state.hover
    });
  }

}

export default compose(withApollo, withRouter)(CurrentUserPanelWidget);