import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { __ } from "react-pe-utilities";
import { get, exec_route } from "react-pe-layouts";

class FreeMenu extends Component {
  render() {
    const menuData = exec_route(get(this.props.menu_id));

    if (!this.props.menu_id) {
      return /*#__PURE__*/React.createElement("div", {
        className: "alert alert-danger"
      }, "Empty free menu data in sector \xABroutes\xBB in layouts");
    }

    if (!menuData) {
      return /*#__PURE__*/React.createElement("div", {
        className: "alert alert-danger"
      }, "No contents in sector \xAB", this.props.menu_id, "\xBB in layouts");
    }

    const menu = menuData.map((e, i) => /*#__PURE__*/React.createElement(NavLink, {
      to: e.route,
      className: "free-menu-item",
      activeClassName: "active",
      key: i
    }, /*#__PURE__*/React.createElement("span", null, __(e.title))));
    console.log(this.props);
    return /*#__PURE__*/React.createElement("div", {
      className: "free-menu-container"
    }, menu);
  }

}

export default FreeMenu;