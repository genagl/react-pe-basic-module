import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { __ } from "react-pe-utilities";

class FreeNavLinkWidget extends Component {
  render() {
    return /*#__PURE__*/React.createElement(NavLink, {
      to: `${this.props.preroute}/${this.props.link_route}`,
      className: this.props.className,
      activeClassName: this.props.activeClassName || "active"
    }, this.props.content, __(this.props.label));
  }

}

export default FreeNavLinkWidget;