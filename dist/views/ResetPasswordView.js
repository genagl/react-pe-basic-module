function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { compose } from "recompose";
import { Mutation, withApollo } from "react-apollo";
import { withRouter } from "react-router";
import { __ } from "react-pe-utilities";
import { mutationToken } from "react-pe-layouts";
import BasicState from "react-pe-basic-view";

class RememberPasswordView extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "passWord", e => {
      this.setState({
        password: e.currentTarget.value
      });
    });

    _defineProperty(this, "addRender", () => /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "row justify-content-center"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-12"
    }, /*#__PURE__*/React.createElement("div", {
      className: "page-title text-center mb-3"
    }, __("Сбросить пароль"))), /*#__PURE__*/React.createElement("div", {
      className: "col-md-4 col-12"
    }, /*#__PURE__*/React.createElement("div", {
      className: "tariff_student p-5"
    }, /*#__PURE__*/React.createElement(Mutation, {
      mutation: mutationToken()
    }, (token, {
      data
    }) => /*#__PURE__*/React.createElement("form", {
      onSubmit: evt => this.onReset(evt, token)
    }, /*#__PURE__*/React.createElement("div", {
      className: "form-group"
    }, /*#__PURE__*/React.createElement("label", {
      htmlFor: "exampleInputPassword1"
    }, __("Старый пароль")), /*#__PURE__*/React.createElement("input", {
      type: "password",
      className: "form-control",
      placeholder: __("Пароль"),
      onChange: this.passWord
    })), /*#__PURE__*/React.createElement("div", {
      className: "form-group"
    }, /*#__PURE__*/React.createElement("label", {
      htmlFor: "exampleInputPassword1"
    }, __("Новый пароль")), /*#__PURE__*/React.createElement("input", {
      type: "password",
      className: "form-control",
      placeholder: __("Пароль"),
      onChange: this.passWord
    })), /*#__PURE__*/React.createElement("div", {
      className: "form-group"
    }, /*#__PURE__*/React.createElement("label", {
      htmlFor: "exampleInputPassword1"
    }, __("Повторите новый пароль")), /*#__PURE__*/React.createElement("input", {
      type: "password",
      className: "form-control",
      placeholder: __("Пароль"),
      onChange: this.passWord
    })), /*#__PURE__*/React.createElement("input", {
      type: "submit",
      className: "btn btn-primary py-2 px-5 rounded-pill",
      value: __("Вход")
    })))))))));

    _defineProperty(this, "getRoute", () => "login");

    _defineProperty(this, "onReset", (evt, token) => {
      evt.preventDefault();
    });
  }

}

export default compose(withApollo, withRouter)(RememberPasswordView);