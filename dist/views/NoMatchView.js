function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import BasicState from "react-pe-basic-view";
import { __ } from "react-pe-utilities";

class NoMatchView extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "myState", () =>
    /*#__PURE__*/
    // console.log(this.props);
    React.createElement("div", {
      className: "row text-center"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-12 my-4"
    }, /*#__PURE__*/React.createElement("div", {
      className: "_404"
    })), /*#__PURE__*/React.createElement("div", {
      className: "col-12 lead"
    }, __("this page no searched")), /*#__PURE__*/React.createElement("div", {
      className: "col-12 my-4"
    }, /*#__PURE__*/React.createElement(Link, {
      className: "btn btn-danger btn-sm",
      to: "/"
    }, __("Return to main page")))));
  }

}

export default withRouter(NoMatchView);