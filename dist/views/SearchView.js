function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { withRouter } from "react-router-dom";
import { __ } from "react-pe-utilities";
import { sprintf } from "react-pe-utilities";
import BasicState from "react-pe-basic-view";

class SearchView extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "addRender", () => {
      console.log(this.props);
      const sres = this.props.match && this.props.match.params && this.props.match.params.id ? this.props.match.params.id : "";
      return /*#__PURE__*/React.createElement("div", {
        className: "container"
      }, /*#__PURE__*/React.createElement("div", {
        className: "row"
      }, /*#__PURE__*/React.createElement("div", {
        className: "col-12"
      }, /*#__PURE__*/React.createElement("div", {
        className: "page-title text-center mb-3"
      }, sprintf(__("Результаты поиска по запросу: %s"), sres))), /*#__PURE__*/React.createElement("div", {
        className: "col-12"
      })));
    });
  }

}

export default withRouter(SearchView);