function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import { compose } from "recompose";
import { withApollo, Mutation } from "react-apollo";
import gql from "graphql-tag";
import BasicState from "react-pe-basic-view";
import { __ } from "react-pe-utilities";

class ChangeEmailView extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "myState", () => {
      const mutation = gql`
			mutation verifyEmailUser($id: String $code: String) {
				verifyEmailUser(id:$id, code:$code)
			}`;
      console.log(this.props.match.params);
      return /*#__PURE__*/React.createElement(Mutation, {
        mutation: mutation
      }, verifyEmailUser => {
        if (!this.state.is_requested) {
          verifyEmailUser({
            variables: {
              id: this.props.match.params.id,
              code: this.props.match.params.code
            },
            update: (store, {
              data
            }) => {
              console.log(data.verifyEmailUser);

              if (data.verifyEmailUser) {
                const state = {
                  is_verified: true,
                  new_email: data.verifyEmailUser
                };
                this.setState(state);
              } else {}

              this.setState({
                is_requested: true
              });
            }
          });
        }

        return this.state.is_verified ? this.success() : this.notsuccess();
      });
    });

    _defineProperty(this, "getRoute", () => "verify");
  }

  success() {
    return /*#__PURE__*/React.createElement("div", {
      className: "row text-center justify-content-center"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-12 my-4"
    }), /*#__PURE__*/React.createElement("div", {
      className: "col-md-7 lead"
    }, __("Адрес Вашей электронной почты изменён. С настоящего момента вход на сайт осуществляется по полю - "), /*#__PURE__*/React.createElement("div", {
      className: "font-weight-bold"
    }, this.state.new_email)), /*#__PURE__*/React.createElement("div", {
      className: "col-12 my-4"
    }, /*#__PURE__*/React.createElement(Link, {
      className: "btn btn-danger btn-sm",
      to: "/"
    }, __("Return to main page"))));
  }

  notsuccess() {
    return /*#__PURE__*/React.createElement("div", {
      className: "row text-center"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-12 my-4"
    }), /*#__PURE__*/React.createElement("div", {
      className: "col-12 lead"
    }, __("Адрес электронной почты не подтвержден. Проверьте ссылку из письма.")), /*#__PURE__*/React.createElement("div", {
      className: "col-12 my-4"
    }, /*#__PURE__*/React.createElement(Link, {
      className: "btn btn-danger btn-sm",
      to: "/"
    }, __("Return to main page"))));
  }

}

export default compose(withApollo, withRouter)(ChangeEmailView);