function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Fragment } from "react";
import { Intent } from "@blueprintjs/core";
import { NavLink, Link } from "react-router-dom";
import { compose } from "recompose";
import { withApollo, Mutation } from "react-apollo";
import { withRouter } from "react-router";
import gql from "graphql-tag";
import BasicState from "react-pe-basic-view";
import { AppToaster } from 'react-pe-useful';
import { __ } from "react-pe-utilities";

class RegisterView extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      before: true
    });

    _defineProperty(this, "email", e => {
      this.setState({
        email: e.currentTarget.value
      });
    });

    _defineProperty(this, "phone", e => {
      this.setState({
        phone: e.currentTarget.value
      });
    });

    _defineProperty(this, "passWord", e => {
      this.setState({
        password: e.currentTarget.value
      });
    });

    _defineProperty(this, "onName", e => {
      this.setState({
        display_name: e.currentTarget.value
      });
    });

    _defineProperty(this, "onSecondName", e => {
      this.setState({
        secondName: e.currentTarget.value
      });
    });

    _defineProperty(this, "onChecked", e => {
      this.setState({
        checked: !this.state.checked
      });
    });

    _defineProperty(this, "render", () => {
      if (this.props.user) {
        return this.no_register();
      }

      return /*#__PURE__*/React.createElement("div", {
        className: "layout-state"
      }, this.state.before ? this.before_register() : this.after_register());
    });

    _defineProperty(this, "getRoute", () => "register");

    _defineProperty(this, "getExtends", () => null);

    _defineProperty(this, "onChange", (state, m_change) => {
      delete state.avatar;
      delete state.html;
      delete state.description;
      delete state.panelHtml;
      delete state.roles;
      delete state.route;
      delete state.sub;
      delete state.__typename;
      console.log(state);

      if (this.state.email === "") {
        AppToaster.show({
          intent: Intent.DANGER,
          icon: "error",
          message: __("email not be empty")
        });
        return;
      }

      if (this.state.name === "") {
        AppToaster.show({
          intent: Intent.DANGER,
          icon: "error",
          message: __("name not be empty")
        });
        return;
      } // if(this.state.password === "")
      // {
      // 	AppToaster.show({
      // 		intent: Intent.DANGER,
      // 		icon: "error",
      // 		message: __("password not be empty")
      // 	});
      // 	return;
      // }


      m_change({
        variables: {
          input: {
            user_login: state.login,
            user_email: state.email,
            first_name: state.display_name,
            last_name: state.secondName,
            password: state.password,
            phone: state.phone
          }
        },
        update: (store, {
          data: {
            registerUser
          }
        }) => {
          this.setState({
            before: false
          });
          AppToaster.show({
            intent: Intent.SUCCESS,
            icon: "tick",
            message: __("Вы зарегистрированы! Проверьте свою электронную почту.")
          });
        }
      });
    });
  }

  before_register() {
    const mutation = gql`
			mutation registerUser($input: UserInput) {
				registerUser(input: $input)
			}`;
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Mutation, {
      mutation: mutation
    }, m_change => /*#__PURE__*/React.createElement("div", {
      className: "container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "row justify-content-center h-100 align-items-center"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-12"
    }, /*#__PURE__*/React.createElement("div", {
      className: "page-title text-center mb-3"
    }, __("Регистрация"))), /*#__PURE__*/React.createElement("div", {
      className: "col-md-4 col-12"
    }, /*#__PURE__*/React.createElement("div", {
      className: "tariff_student p-5"
    }, /*#__PURE__*/React.createElement("form", {
      onSubmit: e => {
        e.preventDefault();
        this.onChange(this.state, m_change);
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: "form-group justify-content-center d-flex flex-column"
    }, /*#__PURE__*/React.createElement("label", {
      className: "exampleInputEmail1"
    }, __("Имя")), /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "form-control",
      placeholder: __("Имя"),
      value: this.state.name,
      onChange: this.onName
    })), /*#__PURE__*/React.createElement("div", {
      className: "form-group justify-content-center d-flex flex-column"
    }, /*#__PURE__*/React.createElement("label", {
      className: "exampleInputEmail1"
    }, __("Фамилия")), /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "form-control",
      placeholder: __("Фамилия"),
      value: this.state.secondName,
      onChange: this.onSecondName
    })), /*#__PURE__*/React.createElement("div", {
      className: "form-group"
    }, /*#__PURE__*/React.createElement("label", {
      htmlFor: "exampleInputEmail1"
    }, __("Электронная почта")), /*#__PURE__*/React.createElement("input", {
      type: "email",
      className: "form-control",
      value: this.state.emain,
      placeholder: __("Эл. Почта"),
      onChange: this.email
    })), /*#__PURE__*/React.createElement("div", {
      className: "form-group"
    }, /*#__PURE__*/React.createElement("label", {
      htmlFor: "exampleInputEmail1"
    }, __("Номер телефона для связи")), /*#__PURE__*/React.createElement("input", {
      type: "phone",
      className: "form-control",
      placeholder: __("Введите номер телефона"),
      value: this.props.phone,
      onChange: this.phone
    })), /*#__PURE__*/React.createElement("div", {
      className: "form-group"
    }, /*#__PURE__*/React.createElement("label", {
      htmlFor: "exampleInputPassword1"
    }, __("Пароль")), /*#__PURE__*/React.createElement("input", {
      type: "password",
      className: "form-control",
      placeholder: __("Пароль"),
      onChange: this.passWord
    })), /*#__PURE__*/React.createElement("div", {
      className: "form-group form-check"
    }, /*#__PURE__*/React.createElement("input", {
      type: "checkbox",
      className: "form-check-input",
      onChange: this.onChecked,
      checked: this.state.checked,
      id: "exampleCheck1"
    }), /*#__PURE__*/React.createElement("label", {
      className: "form-check-label text-left",
      htmlFor: "exampleCheck1"
    }, __("Я согласен с "), /*#__PURE__*/React.createElement(NavLink, {
      to: "/usl"
    }, __("Условиями Пользовательского соглашения")))), /*#__PURE__*/React.createElement("input", {
      type: "submit",
      className: "btn btn-primary py-2 px-5 rounded-pill",
      value: __("Зарегистрироваться")
    }))))))));
  }

  after_register() {
    return /*#__PURE__*/React.createElement("div", {
      className: "row justify-content-center h-100 align-items-center"
    }, /*#__PURE__*/React.createElement("div", {
      className: "lead text-center col-md-6"
    }, /*#__PURE__*/React.createElement("div", {
      className: "font-weight-bold"
    }, __("Внимание! ")), /*#__PURE__*/React.createElement("div", {
      className: "my-5"
    }, __("В ближайшие 10 минут Вы получите электронное письмо на адрес, который Вы указали при регистрации. Для завершения регистрации необходимо совершить последний шаг (инструкция в письме).")), /*#__PURE__*/React.createElement(Link, {
      to: "/",
      className: "btn btn-danger btn-sm"
    }, __("Return to main page"))));
  }

  no_register() {
    return /*#__PURE__*/React.createElement("div", {
      className: "row justify-content-center h-100 align-items-center"
    }, /*#__PURE__*/React.createElement("div", {
      className: "lead text-center col-md-6"
    }, __("Для регистрации новой учётной записи необходимо разлогиниться.")), /*#__PURE__*/React.createElement(Link, {
      to: "/",
      className: "btn btn-danger btn-sm mt-5"
    }, __("Return to main page")));
  }

}

export default compose(withApollo, withRouter)(RegisterView);