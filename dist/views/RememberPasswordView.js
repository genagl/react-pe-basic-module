function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { compose } from "recompose";
import gql from "graphql-tag";
import { Mutation, withApollo } from "react-apollo";
import { withRouter } from "react-router";
import { Intent } from "@blueprintjs/core";
import { Link } from "react-router-dom";
import { __ } from "react-pe-utilities";
import { AppToaster } from 'react-pe-useful';
import BasicState from "react-pe-basic-view";

class RememberPasswordView extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onEmail", e => {
      this.setState({
        email: e.currentTarget.value,
        alert: null
      });
    });

    _defineProperty(this, "addRender", () => {
      const mutation_remember = gql`mutation restorePass( $email:String )
		{
		  restorePass( email:$email )
		}`;
      return /*#__PURE__*/React.createElement("div", {
        className: "row justify-content-center"
      }, /*#__PURE__*/React.createElement("div", {
        className: "col-md-4 col-12"
      }, /*#__PURE__*/React.createElement("div", {
        className: "tariff_student p-5"
      }, /*#__PURE__*/React.createElement(Mutation, {
        mutation: mutation_remember
      }, (token, {
        data
      }) => /*#__PURE__*/React.createElement("form", {
        onSubmit: evt => this.onRemember(evt, token)
      }, /*#__PURE__*/React.createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/React.createElement("label", null, __("Insert your e-mail")), /*#__PURE__*/React.createElement("input", {
        type: "string",
        className: "form-control",
        placeholder: __("e-mail"),
        onChange: this.onEmail
      }), /*#__PURE__*/React.createElement("div", {
        className: "h-70"
      }, /*#__PURE__*/React.createElement("div", {
        className: this.state.alert ? "text-danger" : "hidden"
      }, __("email not be empty")))), /*#__PURE__*/React.createElement("input", {
        type: "submit",
        className: "btn btn-primary py-2 px-4 my-2 rounded-pill",
        value: __("Send instructions to e-mail")
      }), /*#__PURE__*/React.createElement(Link, {
        className: "btn btn-primary py-2 px-4 my-2 rounded-pill",
        to: "/"
      }, __("or return to main page")))))));
    });

    _defineProperty(this, "getRoute", () => "remember");

    _defineProperty(this, "onRemember", (evt, restorePass) => {
      evt.preventDefault();

      if (this.state.email) {
        restorePass({
          variables: {
            email: this.state.email
          },
          update: (store, data) => {
            if (data.data.restorePass) {
              AppToaster.show({
                intent: Intent.SUCCESS,
                icon: "tick",
                message: __("Проверьте свою электронную почту и следуйте инструкциям.")
              });
            } else {
              AppToaster.show({
                intent: Intent.DANGER,
                icon: "tick",
                message: __("Полььзователя с указанной почтой не найдено. Попробуйте ввести другой e-mail адрес")
              });
            }
          }
        });
      } else {
        this.setState({
          alert: true
        });
      }
    });
  }

}

export default compose(withApollo, withRouter)(RememberPasswordView);