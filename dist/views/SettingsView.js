function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import BasicState from "react-pe-basic-view";
import { __ } from "react-pe-utilities";
import { styles, currentStyles } from "react-pe-layouts";

class SettingsView extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onChange", evt => {
      const a = evt.currentTarget.getAttribute("e");
      let active = 1;
      const styles1 = styles();

      for (const i in styles1) {
        if (styles1[i].url == a) {
          active = styles1[i]._id;
          break;
        }
      }

      this.setState({
        active
      });
      this.props.onChangeStyle({
        fluid: this.state.fluid,
        style: a
      });
    });

    _defineProperty(this, "onFluid", () => {
      this.setState({
        fluid: !this.state.fluid
      });
      const styles1 = styles();
      this.props.onChangeStyle({
        fluid: !this.state.fluid,
        style: styles1.filter(e => e._id == this.state.active)[0].url
      });
    });
  }

  basic_state_data() {
    let active = 1;
    const styles1 = styles();

    for (const i in styles1) {
      if (styles1[i].url == currentStyles()) {
        console.log(i);
        active = styles1[i]._id;
        break;
      }
    }

    return {
      active,
      fluid: 1
    };
  }

  render() {
    const styles1 = styles();
    const divs = styles1.map((e, i) => /*#__PURE__*/React.createElement("div", {
      className: `btn square btn-secondary  m-1${this.state.active == e._id ? " active" : ""}`,
      onClick: this.onChange,
      e: e.url,
      key: i
    }, /*#__PURE__*/React.createElement("svg", {
      version: "1.1",
      xmlns: "http://www.w3.org/2000/svg",
      x: "0px",
      y: "0px",
      viewBox: "0 0 1 1"
    }, /*#__PURE__*/React.createElement("rect", {
      x: "0",
      y: "0",
      width: "1",
      height: "1"
    })), /*#__PURE__*/React.createElement("div", null, __(e.title))));
    return /*#__PURE__*/React.createElement("div", {
      className: "layout-state"
    }, /*#__PURE__*/React.createElement("div", {
      className: "layout-state-head"
    }, /*#__PURE__*/React.createElement("span", {
      className: `layout-state-logo ${this.state.route.icon}`
    }), /*#__PURE__*/React.createElement("div", {
      className: "layout-state-title"
    }, __(this.state.route.title))), /*#__PURE__*/React.createElement("div", {
      className: "row text-center"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-12 my-2"
    }, /*#__PURE__*/React.createElement("div", {
      className: "lead"
    }, __("Interface styles"))), /*#__PURE__*/React.createElement("div", {
      className: "col-12 my-2"
    }, /*#__PURE__*/React.createElement("div", {
      className: "btn-group d-flex flex-wrap",
      style: {
        justifyContent: "center",
        alignItems: "center"
      }
    }, divs)), /*#__PURE__*/React.createElement("div", {
      className: "col-12 my-2"
    }, /*#__PURE__*/React.createElement("div", {
      className: "lead"
    }, __("Container"))), /*#__PURE__*/React.createElement("div", {
      className: "col-12 my-2"
    }, /*#__PURE__*/React.createElement("label", {
      className: "_check_"
    }, /*#__PURE__*/React.createElement("input", {
      type: "checkbox",
      checked: this.state.fluid,
      onChange: this.onFluid
    })))));
  }

}

export default SettingsView;