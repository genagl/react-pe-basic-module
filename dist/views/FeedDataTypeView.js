function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button, Tag } from "@blueprintjs/core";
import { compose } from "recompose";
import { withApollo } from "react-apollo";
import { withRouter } from "react-router";
import { schema } from "react-pe-layouts";
import BasicState from "react-pe-basic-view";
import { __ } from "react-pe-utilities";
import Moment from "react-moment"; //import Feed from "../../../layouts/BasicState/Feed"

import { Feed } from "react-pe-basic-view";
import { getSingleRoute } from "react-pe-layouts";
import { Link } from "react-router-dom";
import { data_type_link_url } from "react-pe-utilities";

class FeedDataTypeView extends BasicState {
  basic_state_data() {
    //const { feed_data_type } = this.props
    return {
      count: 10,
      offset: 0,
      feed_data_type: this.props.feed_data_type,
      feed_data_type_parent_id: this.props.feed_data_type_parent_id
    };
  }

  stateDidMount() {}

  componentDidUpdate(nextProps) {
    if (nextProps.feed_data_type !== this.state.feed_data_type || nextProps.feed_data_type_parent_id !== this.state.feed_data_type_parent_id) {
      this.setState(nextProps);
    }
  }

  addRender() {
    //console.log(this.props, this.state.feed_data_type_parent_id)
    const {
      offset,
      count,
      height
    } = this.state;
    const {
      feed_data_type,
      feed_data_type_parent_id
    } = this.state;
    const paging = feed_data_type_parent_id ? ` parent: "${feed_data_type_parent_id}" ` : ``;
    return /*#__PURE__*/React.createElement("div", {
      className: "p-0 d-flex w-100 "
    }, /*#__PURE__*/React.createElement(Feed, {
      component: _Component,
      data_type: feed_data_type,
      is_hide_pagi: false,
      offset: offset || 0,
      count: count,
      height: height,
      paging: paging,
      class_name: ` row `,
      params: {
        col: 2
      }
    }));
  }

}

export default compose(withRouter, withApollo)(FeedDataTypeView);
export class _Component extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "goto", link => {
      alert(link);
    });

    this.state = { ...this.props
    };
  }

  render() {
    let tags = [];
    const routeSingle = getSingleRoute(this.state.__typename);

    const data_scheme = schema()[this.state.__typename];

    const link = routeSingle ? routeSingle.route : null;
    const visibled_value = data_scheme.visibled_value ? data_scheme.visibled_value : "id"; //console.log( this.props )

    Object.keys(this.state).forEach((tax, i) => {
      if (!Array.isArray(this.state[tax]) || this.state[tax].length === 0 || data_scheme.apollo_fields[tax].filter_type !== "taxonomies") return; //
      // ищем в схеме тип данных у всех массивов state'a

      const schemaElement = Object.keys(schema()).filter((data_type, index) => {
        return data_type === this.state[tax][0].__typename;
      })[0]; // нашли Тип Данных.

      if (schemaElement) {
        //console.log(schemaElement)
        this.state[tax].forEach((tag, ii) => {
          const visibled_value = schema()[schemaElement].visibled_value ? schema()[schemaElement].visibled_value : tag.id;
          tags.push( /*#__PURE__*/React.createElement(Link, {
            to: "/" + data_type_link_url(schemaElement) + "/" + tag.id
          }, /*#__PURE__*/React.createElement(Tag, {
            key: ii + Math.random(),
            style: {
              backgroundColor: schema()[schemaElement].admin_data.fill ? schema()[schemaElement].admin_data.fill[1] : "#444",
              cursor: "pointer"
            },
            title: __(schema()[schemaElement].name)
          }, tag[visibled_value])));
        });
      }
    });
    var regex = /(<([^>]+)>)/ig;
    var postDateBlock = this.state.post_date ? /*#__PURE__*/React.createElement("div", {
      className: "post-date"
    }, /*#__PURE__*/React.createElement(Moment, {
      locale: "ru",
      format: "D.MM.YYYY "
    }, new Date(this.state.post_date))) : null;
    const linkBlock = link ? /*#__PURE__*/React.createElement(Link, {
      to: "/" + link + "/" + this.state.id,
      className: "ml-auto"
    }, /*#__PURE__*/React.createElement(Button, {
      minimal: true,
      rightIcon: "chevron-right"
    }, __("More"))) : null;
    let thumb = "thumbnail";

    switch (this.state.__typename) {
      case "User":
        thumb = "avatar";
        break;

      default:
        thumb = "thumbnail";
    }

    let orderLabel;

    if (this.props.order) {
      orderLabel = /*#__PURE__*/React.createElement("div", {
        className: "single-data-type-order position-absolute "
      }, this.props.order);
    }

    const linkBack = link ? /*#__PURE__*/React.createElement(Link, {
      to: "/" + link + "/" + this.state.id,
      className: "ml-auto"
    }, /*#__PURE__*/React.createElement("div", {
      className: "post-thumbnail m-0",
      style: {
        backgroundImage: "url(" + this.state[thumb] + ")"
      }
    })) : /*#__PURE__*/React.createElement("div", {
      className: "post-thumbnail m-0",
      style: {
        backgroundImage: "url(" + this.state[thumb] + ")"
      }
    });
    let colClass = " col-12 ";

    if (this.state.col) {
      switch (this.state.col) {
        case 2:
          colClass = " col-md-6 ";
          break;

        case 3:
          colClass = " col-md-4 ";
          break;

        case 4:
          colClass = " col-md-3 ";
          break;

        case 1:
        default:
          colClass = " col-12 ";
          break;
      }
    }

    return /*#__PURE__*/React.createElement("div", {
      className: colClass + " " + this.state.cardClass
    }, /*#__PURE__*/React.createElement("div", {
      className: "card p-0 d-flex flex-row h-100"
    }, linkBack, orderLabel, /*#__PURE__*/React.createElement("div", {
      className: "d-flex flex-column p-4 flex-grow-100"
    }, /*#__PURE__*/React.createElement("h5", null, this.state[visibled_value]), /*#__PURE__*/React.createElement("div", null, tags), /*#__PURE__*/React.createElement("div", {
      className: "py-3"
    }, this.state.post_content ? this.state.post_content.replace(regex, "").substring(0, 200) : "", "..."), /*#__PURE__*/React.createElement("div", {
      className: "d-flex mt-auto align-items-end"
    }, postDateBlock, linkBlock))));
  }

}