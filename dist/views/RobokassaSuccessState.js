function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { Link } from "react-router-dom";
import BasicState from "react-pe-basic-view";
import { __ } from "react-pe-utilities";

class RobokassaSuccessState extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "myState", () => /*#__PURE__*/React.createElement("div", {
      className: "row text-center"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-12 my-4"
    }, "\u041E\u043F\u043B\u0430\u0442\u0430 \u043F\u0440\u043E\u0448\u043B\u0430 \u0443\u0441\u043F\u0435\u0448\u043D\u043E!"), /*#__PURE__*/React.createElement("div", {
      className: "col-12 my-4"
    }, /*#__PURE__*/React.createElement(Link, {
      className: "btn btn-danger btn-sm",
      to: "/"
    }, __("Return to main page")))));
  }

}

export default RobokassaSuccessState;