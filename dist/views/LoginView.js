function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { compose } from "recompose";
import { Mutation, withApollo } from "react-apollo";
import { withRouter } from "react-router";
import { Intent } from "@blueprintjs/core";
import { NavLink } from "react-router-dom";
import { __ } from "react-pe-utilities";
import { mutationToken, queryUserInfo } from "react-pe-layouts";
import BasicState from "react-pe-basic-view";
import { AppToaster } from 'react-pe-useful';
import { UserContext } from "react-pe-layout-app";

class LoginView extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "login", e => {
      this.setState({
        login: e.currentTarget.value
      });
    });

    _defineProperty(this, "passWord", e => {
      this.setState({
        password: e.currentTarget.value
      });
    });

    _defineProperty(this, "addRender", () => /*#__PURE__*/React.createElement("div", {
      className: "container"
    }, /*#__PURE__*/React.createElement("div", {
      className: "row justify-content-center"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-12"
    }, /*#__PURE__*/React.createElement("div", {
      className: "page-title text-center mb-3"
    }, __("Вход"))), /*#__PURE__*/React.createElement("div", {
      className: "col-md-4 col-12"
    }, /*#__PURE__*/React.createElement("div", {
      className: "tariff_student p-5"
    }, /*#__PURE__*/React.createElement(Mutation, {
      mutation: mutationToken()
    }, (token, {
      data
    }) => /*#__PURE__*/React.createElement(UserContext.Consumer, null, context => {
      return /*#__PURE__*/React.createElement("form", {
        onSubmit: evt => this.onLogin(evt, token, context)
      }, /*#__PURE__*/React.createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/React.createElement("label", {
        htmlFor: "exampleInputEmail1"
      }, __("Эл. Почта")), /*#__PURE__*/React.createElement("input", {
        type: "text",
        className: "form-control",
        placeholder: __("Эл. Почта"),
        onChange: this.login
      })), /*#__PURE__*/React.createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/React.createElement("label", {
        htmlFor: "exampleInputPassword1"
      }, __("Пароль")), /*#__PURE__*/React.createElement("input", {
        type: "password",
        className: "form-control",
        placeholder: __("Пароль"),
        onChange: this.passWord
      })), /*#__PURE__*/React.createElement("div", {
        className: "form-group form-check"
      }, /*#__PURE__*/React.createElement("label", {
        className: "form-check-label text-left",
        htmlFor: "exampleCheck1"
      }, __("Если у вас нет профиля, то вы можете: "), /*#__PURE__*/React.createElement(NavLink, {
        to: "/register",
        className: "text-center"
      }, __("Зарегистрироваться")))), /*#__PURE__*/React.createElement("div", {
        className: "form-group form-check"
      }, /*#__PURE__*/React.createElement("label", {
        className: "form-check-label text-left",
        htmlFor: "exampleCheck1"
      }, __("Если вы забыли пароль: "), /*#__PURE__*/React.createElement(NavLink, {
        to: "/remember",
        className: "text-center"
      }, __("Напомнить пароль")))), /*#__PURE__*/React.createElement("input", {
        type: "submit",
        className: "btn btn-primary py-2 px-5 rounded-pill",
        value: __("Вход")
      }));
    })))))));

    _defineProperty(this, "onLogin", (evt, token, context) => {
      evt.preventDefault();
      token({
        variables: {
          input: {
            grant_type: "wp-ciba",
            login: this.state.login || "",
            password: this.state.password || ""
          }
        },
        update: (store, {
          data
        }) => {
          if (data.token) {
            console.log(data);
            AppToaster.show({
              intent: Intent.SUCCESS,
              icon: "tick",
              message: __("You enter by User")
            });
            localStorage.setItem("token", data.token.access_token); // context.setUser( queryUserInfo() );
            // this.props.history.replace("/")

            this.props.client.query({
              query: queryUserInfo(),
              fetchPolicy: 'network-only'
            }).then(result => {
              context.setUser(result.data.userInfo);
              this.props.history.replace('/');
            });
          } else {}
        },
        refetchQueries: [{
          query: queryUserInfo(),
          variables: {}
        }]
      });
    });
  }

}

export default compose(withApollo, withRouter)(LoginView);