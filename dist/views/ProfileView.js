function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { Dialog, Intent } from "@blueprintjs/core";
import gql from "graphql-tag";
import { withApollo, Mutation } from "react-apollo";
import { compose } from "recompose";
import { withRouter } from "react-router";
import { AppToaster } from 'react-pe-useful';
import FieldInput from "react-pe-scalars";
import BasicState from "react-pe-basic-view";
import { __, initArea } from "react-pe-utilities";
import { CircularProgressbarWithChildren } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { LayoutIcon } from 'react-pe-useful';

class ProfileView extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "handleToggle", () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    });

    _defineProperty(this, "onFirstName", e => {
      this.onField(e, "first_name");
      console.log(e);
    });

    _defineProperty(this, "SecondName", e => {
      this.onField(e, "last_name");
    });

    _defineProperty(this, "onEmail", e => {
      this.onField(e, "user_email");
    });

    _defineProperty(this, "onPhone", e => {
      this.onField(e, "phone");
    });

    _defineProperty(this, "onPassword", e => {
      this.onField(e, "password");
    });

    _defineProperty(this, "onField", (e, name) => {
      this.state.form[name] = e;
      this.setState({
        form: { ...this.state.form
        }
      });
    });

    _defineProperty(this, "onChange", m_change => {
      const form = { ...this.state.form
      };
      delete form.current_course;
      delete form.payments;
      delete form.display_name;
      delete form.id;
      delete form.__typename;
      delete form.roles; // delete form.user_email;

      m_change({
        variables: {
          /*
          input: {
          user_email: this.state.form.user_login,
          display_name: this.state.form.display_name + " " + this.state.form.second_name,
          password: this.state.form.password,
          }
          */
          input: form
        },
        update: (store, {
          data: {
            changeCurrentUser
          }
        }) => {
          switch (changeCurrentUser) {
            case 2:
              this.setState({
                isOpen: true
              });
              break;

            case 1:
            default:
              AppToaster.show({
                intent: Intent.SUCCESS,
                icon: "tick",
                message: __("Profile was updated")
              });
              break;
          }
        }
      });
    });
  }

  basic_state_data() {
    return {
      form: this.props.user ? this.props.user : {}
    };
  }

  addRender() {
    const mutation = gql`
      mutation changeCurrentUser($input: UserInput) {
        changeCurrentUser(input: $input)
      }
    `;
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Mutation, {
      mutation: mutation
    }, m_change => {
      // console.log(this.props.user);
      console.log(this.state);
      return /*#__PURE__*/React.createElement("div", {
        className: " mt-5"
      }, /*#__PURE__*/React.createElement("div", {
        className: "tutor-row"
      }, /*#__PURE__*/React.createElement("div", {
        className: "tutor-left-aside mobile-relatived"
      }, initArea("user-favorites-left-aside", { ...this.props
      }, /*#__PURE__*/React.createElement(CircularProgressbarWithChildren, {
        value: 81,
        styles: {
          // Customize the root svg element
          root: {},
          // Customize the path, i.e. the "completed progress"
          path: {
            // Path color
            stroke: "#175586",
            // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
            strokeLinecap: "butt",
            // Customize transition animation
            transition: "stroke-dashoffset 0.5s ease 0s",
            // Rotate the path
            transform: "rotate(0.25turn)",
            transformOrigin: "center center"
          },
          // Customize the circle behind the path, i.e. the "total progress"
          trail: {
            // Trail color
            stroke: "#17558600",
            // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
            strokeLinecap: "butt",
            // Rotate the trail
            transform: "rotate(0.25turn)",
            transformOrigin: "center center"
          },
          // Customize the text
          text: {
            // Text color
            fill: "#f88",
            // Text size
            fontSize: "16px"
          },
          // Customize background - only used when the `background` prop is true
          background: {
            fill: "#3e98c700"
          }
        }
      }, /*#__PURE__*/React.createElement(LayoutIcon, {
        src: "assets/img/user1.svg",
        className: "profile-ava"
      })))), /*#__PURE__*/React.createElement("div", {
        className: "tutor-main"
      }, /*#__PURE__*/React.createElement("div", {
        className: " p-0"
      }, /*#__PURE__*/React.createElement("form", {
        onSubmit: e => {
          e.preventDefault();
          this.onChange(m_change);
        }
      }, /*#__PURE__*/React.createElement(FieldInput, {
        title: "\u0418\u043C\u044F",
        type: "string",
        field: "first_name",
        editable: true,
        value: this.state.form.first_name,
        onChange: this.onFirstName
      }), /*#__PURE__*/React.createElement(FieldInput, {
        title: "\u0424\u0430\u043C\u0438\u043B\u0438\u044F",
        type: "string",
        field: "last_name",
        editable: true,
        value: this.state.form.last_name,
        onChange: this.SecondName
      }), /*#__PURE__*/React.createElement(FieldInput, {
        title: "E-mail",
        type: "email",
        field: "user_email",
        editable: true,
        value: this.state.form.user_email,
        onChange: this.onEmail
      }), /*#__PURE__*/React.createElement(FieldInput, {
        title: "\u0422\u0435\u043B\u0435\u0444\u043E\u043D \u0434\u043B\u044F \u0441\u0432\u044F\u0437\u0438",
        type: "phone",
        field: "phone",
        editable: true,
        value: this.state.form.phone,
        onChange: this.onPhone
      }), /*#__PURE__*/React.createElement(FieldInput, {
        title: "\u041D\u043E\u0432\u044B\u0439 \u043F\u0430\u0440\u043E\u043B\u044C",
        type: "password",
        field: "password",
        editable: true,
        value: this.state.form.password,
        onChange: this.onPassword
      }), /*#__PURE__*/React.createElement("div", {
        className: "row"
      }, /*#__PURE__*/React.createElement("div", {
        className: "col-md-7 offset-md-3 mt-5 text-center"
      }, /*#__PURE__*/React.createElement("input", {
        type: "submit",
        className: "btn btn-secondary my-3",
        value: __("Сохранить")
      }))))))), /*#__PURE__*/React.createElement("div", {
        className: "tutor-right-aside"
      }, initArea("user-favorites-right-aside", { ...this.props
      })));
    }), /*#__PURE__*/React.createElement(Dialog, {
      isOpen: this.state.isOpen,
      onClose: this.handleToggle,
      title: __("Attention")
    }, /*#__PURE__*/React.createElement("div", {
      className: "px-5 pb-5"
    }, __("Instructions have been sent to your old email address for changing the «email address» field. Changes will take effect only after the verification procedure is completed."))));
  }

}

export default compose(withApollo, withRouter)(ProfileView);