import ChangeEmailView from "./views/ChangeEmailView";
import FeedDataTypeView from "./views/FeedDataTypeView";
import LoginView from "./views/LoginView";
import ProfileView from "./views/ProfileView";
import ResetPasswordView from "./views/ResetPasswordView";
import NoMatchView from "./views/NoMatchView";
import FinishRestorePasswordView from "./views/FinishRestorePasswordView";
import HTMLSourceVew from "./views/HTMLSourceVew";
import HTMLView from "./views/HTMLView";
import RegisterView from "./views/RegisterView";
import RememberPasswordView from "./views/RememberPasswordView"; // import RobokassaFailureState from "./views/RobokassaFailureState"
// import RobokassaSuccessState from "./views/RobokassaSuccessState"

import SearchView from "./views/SearchView";
import SettingsView from "./views/SettingsView";
import SingledDataTypeView from "./views/SingledDataTypeView";
import VerifyUserView from "./views/VerifyUserView";
import Aside from "./widgets/Aside";
import CurrentUserPanelWidget from "./widgets/CurrentUserPanelWidget";
import FreeHTMLWidget from "./widgets/FreeHTMLWidget";
import FreeMenu from "./widgets/FreeMenu";
import FreeNavLinkWidget from "./widgets/FreeNavLinkWidget";
import FreeRouteWidget from "./widgets/FreeRouteWidget";
import PagiWidget from "./widgets/PagiWidget";
import PEExporter from "./widgets/PEExporter";
import SearchBlock from "./widgets/SearchBlock";
import UsersLockLabelWidget from "./widgets/UsersLockLabelWidget";
export { ChangeEmailView, FeedDataTypeView, HTMLSourceVew, HTMLView, RegisterView, RememberPasswordView, //RobokassaFailureState,
//RobokassaSuccessState,
SearchView, SettingsView, SingledDataTypeView, VerifyUserView, LoginView, ProfileView, ResetPasswordView, NoMatchView, FinishRestorePasswordView, Aside, CurrentUserPanelWidget, FreeHTMLWidget, FreeMenu, FreeNavLinkWidget, FreeRouteWidget, PagiWidget, PEExporter, SearchBlock, UsersLockLabelWidget };