# Protopia Ecosystems Basic Module

Основной [модуль](http://ux.protopia-home.ru/modules/) для [ProtopiaEcosystem React Client](http://ux.protopia-home.ru/). Включает в себя набор Экранов, Виджетов, настроек и ассетов, без которых приложение скомпеллируется корректно.

Данный пакет автоматически устанавливается в **Create-react-app приложение**

